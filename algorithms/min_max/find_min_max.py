#!/usr/bin/env python3

"""Simultanously find the smallest and the largest element in a given array

The number of comparisons should be at most 3 * ⌊n/2⌋ where n is the number of
elements in the array.
"""

import random

# Each element of the tuple below has the following form:
# [<elements>], (<expected_min, expected_max>)]
TEST_ARRAYS_FIND_MIN_MAX = (
    # Empty array.
    ([], (None, None)),

    # One element.
    ([0], (0, 0)),
    ([1], (1, 1)),

    # Two elements.
    ([1, 2], (1, 2)),
    ([2, 1], (1, 2)),

    # Three elements.
    ([1, 2, 3], (1, 3)),
    ([1, 3, 2], (1, 3)),
    ([2, 3, 1], (1, 3)),
    ([2, 1, 3], (1, 3)),
    ([3, 1, 2], (1, 3)),
    ([3, 2, 1], (1, 3)),

    # Many elements.
    (list(range(100)), (0, 99)),
    (list(range(99, -1, -1)), (0, 99)),

    # Many elements in random order.
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),
    (random.sample(range(100), 100), (0, 99)),

    # Even more elements in random order.
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
    (random.sample(range(1_000_000), 1_000_000), (0, 999_999)),
)


def main():
    """Main entry point for this module, test the algorithm"""
    for array, expected in TEST_ARRAYS_FIND_MIN_MAX:
        # Compute the total expected amount of comparisons from the number of
        # elements in the array.
        n_comp_max = 3 * (len(array) // 2)
        test_find_min_max(array, expected, n_comp_max)


def find_min_max(array):
    """Simultanously find the minimum and maximum element in a given array

    Perfom at most 3 * ⌊n/2⌋ comparisons where n is the number of elements in
    the array.

    Args:
        array: A list containing elements to be compared

    Returns:
        The smallest element in the array.
        The largest element in the array.
        The total number of comparisons performed.
    """
    if not array:
        return None, None, 0

    len_a = len(array)

    first = array[0]

    if not len_a % 2:
        start = 2
        second = array[1]
        if first < second:
            smallest = first
            largest = second
        else:
            smallest = second
            largest = first
    else:
        start = 1
        smallest = first
        largest = first

    n_comp = 0
    for i in range(start, len_a - 1, 2):
        # Pairwise comparison of elements.
        a_i = array[i]
        a_j = array[i + 1]

        n_comp += 1
        if a_i < a_j:
            n_comp += 1
            if a_i < smallest:
                smallest = a_i

            n_comp += 1
            if a_j > largest:
                largest = a_j
        else:
            n_comp += 1
            if a_i > largest:
                largest = a_i

            n_comp += 1
            if a_j < smallest:
                smallest = a_j

    return smallest, largest, n_comp


def test_find_min_max(array, expected, expected_n_comp_max):
    """Test the find_min_max method"""
    expected_smallest = expected[0]
    expected_largest = expected[1]

    smallest, largest, n_comp = find_min_max(array)

    assert smallest == expected_smallest, (
        f"Expected smallest element to be {expected_smallest} when calling "
        f"'find_min_max(array={array})', but got {smallest}."
    )

    assert largest == expected_largest, (
        f"Expected largest element to be {expected_largest} when calling "
        f"'find_min_max(array={array})', but got {largest}."
    )

    assert n_comp <= expected_n_comp_max, (
        f"Expected maximum number of comparisons to be {expected_n_comp_max} "
        f"when calling 'find_min_max(array={array})', but got {n_comp}."
    )


if __name__ == '__main__':
    main()
