#!/usr/bin/env python3

"""Find smallest and second smallest elements in a given array.

As an additional constraint, the number of comparisons must be less than or
equal to

    n + lg⌈n⌉ - 2

where n is the number of elements in the array and lg is the base-2 logarithm.
"""

import math
import random


# Each element of the tuple of elements to be tested has the following form:
# (<left_tuple>, <right_tuple>, <expected_tuple>)
# Where <left_tuple> and <right_tuple> each have the form:
# (<element>, [<losers>])
# and <element> are the elements to be compared, and [<losers>] is the list of
# elements that have lost previous comparisons.
# <expected_tuple> is a tuple that has the form
# (<winner>, [<losers>])
# and contains the expected winner for each comparison and the updated list of
# losers for that winner.
TEST_TUPLES_COMPARE = (
    # Same entries.
    ((1, []), (1, []), (1, [])),

    # Empty list of losers.
    ((1, []), (3, []), (1, [3])),
    ((3, []), (1, []), (1, [3])),

    # Only one tuple has losers.
    ((1, []), (3, [4]), (1, [3])),
    ((1, [2]), (3, []), (1, [2, 3])),

    # One loser each.
    ((1, [2]), (3, [4]), (1, [2, 3])),
    ((1, [4]), (3, [2]), (1, [4, 3])),

    # Multiple losers.
    ((5, [6, 7, 8]), (2, [3, 4, 9]), (2, [3, 4, 9, 5])),
    ((2, [6, 7, 8]), (5, [3, 4, 9]), (2, [6, 7, 8, 5])),
)

# Each element of the tuple of elements to be tested has the following form:
# ([<candidate_tuple>], ([<winner_tuple>], <number_of_comparisons>))
# where <candidate_tuple> and <winner_tuple> have the form:
# (<element>, [<losers>])
TEST_ARRAYS_FIND_SMALLEST = (
    # Empty array, should do no comparisons.
    ([], ([], 0)),

    # One element in the array.
    ([(1, [])], ([(1, [])], 0)),
    ([(1, [2])], ([(1, [2])], 0)),

    # Two elements to compare.
    ([(1, []), (2, [])], ([(1, [2])], 1)),
    ([(1, []), (2, [4])], ([(1, [2])], 1)),
    ([(1, [3]), (2, [])], ([(1, [3, 2])], 1)),
    ([(1, [3]), (2, [4])], ([(1, [3, 2])], 1)),

    # Three elements to compare.
    ([(3, []), (1, []), (2, [])], ([(2, [3]), (1, [])], 1)),
    ([(3, [4]), (1, [5]), (2, [6])], ([(2, [6, 3]), (1, [5])], 1)),
    ([(3, [4]), (1, [5, 7]), (2, [])], ([(2, [3]), (1, [5, 7])], 1)),

    # Four elements to compare.
    ([(3, []), (1, []), (4, []), (2, [])], ([(2, [3]), (1, [4])], 2)),
    ([(3, []), (1, [6]), (4, [7]), (2, [8])], ([(2, [8, 3]), (1, [6, 4])], 2)),
)

# Each element of the tuple of elements to be tested has the following form:
# ([<element>], (<min_1>, <min_2>)
# where <element> is the element of the array to be tested, <min_1> is the
# expected smallest element, and <min_2> is the expected second smallest
# element in the given array.
TEST_ARRAYS_FIND_MINIMUMS = (
    # Empty array.
    ([], (None, None)),

    # One element in the array.
    ([1], (1, None)),

    # Two elements in the array.
    ([1, 2], (1, 2)),

    # Three elements in the array.
    ([1, 2, 3], (1, 2)),

    # Four elements in the array.
    ([1, 2, 3, 4], (1, 2)),

    # Five elements in the array.
    ([1, 2, 3, 4, 5], (1, 2)),

    # Ten elements in the array.
    ([9, 7, 8, 6, 1, 2, 3, 4, 5, 10], (1, 2)),

    # 50 elements in ascending order.
    (list(range(50)), (0, 1)),

    # 100 elements in descending order.
    (list(range(100, 0, -1)), (1, 2)),

    # 100 elements in random order.
    (random.sample(range(100), 100), (0, 1)),
    (random.sample(range(100), 100), (0, 1)),
    (random.sample(range(100), 100), (0, 1)),
    (random.sample(range(100), 100), (0, 1)),
    (random.sample(range(100), 100), (0, 1)),
    (random.sample(range(100), 100), (0, 1)),
)


def main():
    """Main entry point for this module, test the algorithm"""
    for tup_left, tup_right, expected in TEST_TUPLES_COMPARE:
        test_compare(tup_left, tup_right, expected)

    for array, expected in TEST_ARRAYS_FIND_SMALLEST:
        test_find_smallest(array, expected)

    for array, expected_mins in TEST_ARRAYS_FIND_MINIMUMS:
        len_a = len(array)
        expected_n_comp = 0
        if len_a > 1:
            # Derive the maximum number of comparisons from the length of the
            # given array.
            expected_n_comp = len_a + math.ceil(math.log(len_a, 2)) - 2
        test_find_minimums(array, expected_mins, expected_n_comp)


def find_minimums(array):
    """Find the smallest and the second smallest element in the given array

    The method also counts the number of comparisons performed.

    Args:
        array: List of elements to be compared.

    Returns:
        The smallest element in the array.
        The second smallest element in the array.
        The total number of comparisons performed.
    """
    if not array:
        return None, None, 0

    if len(array) == 1:
        return array[0], None, 0

    # Transform the list of elements to be compared into tuples.  The second
    # element of the tuple will hold the losers of each comparison.
    candidates = [(x, []) for x in array]

    n_comp_1 = 0
    while len(candidates) > 1:
        candidates, n_comp = find_smallest(candidates)
        n_comp_1 += n_comp

    min_1 = candidates[0][0]
    losers = candidates[0][1]

    n_comp_2 = 0
    min_2 = losers[0]
    for i in range(1, len(losers)):
        n_comp_2 += 1
        if losers[i] < min_2:
            min_2 = losers[i]

    return min_1, min_2, n_comp_1 + n_comp_2


def find_smallest(array):
    """Find the smallest elements in a given array, append losers to lists

    Compares elements in given array pair-wise and constructs a list of tuples,
    each containing one winner of the comparison and a list of losers for each
    winner.

    Args:
        array: A list in which each element is a tuple containing the element
           to be compared and a list of losers of previous comparisons.  The
           list has the form:
           array =  [(1, [<losers>]), (2, [<losers>]), ...]

    Returns:
        A list of tuples each containing the winner of each comparison and the
        list of losers up until this point.
        The number of comparisons that have been performed in a call.
    """
    len_a = len(array)

    winners = []
    n_comp = 0

    if len_a == 0:
        return winners, n_comp

    if len_a == 1:
        return array, n_comp

    mid = len_a // 2

    # Start comparison by comparing first and last element and then move
    # "inwards".
    for i in range(mid):
        j = - i - 1

        n_comp += 1
        winner, losers = compare(array[i], array[j])
        winners.append((winner, losers))

        i += 1

    # Take care of middle element (without performing an additional comparison)
    # when the array has an odd length.
    if len_a % 2:
        winners.append(array[mid])

    return winners, n_comp


def compare(left_tup, right_tup):
    """Compare the first elements of two tuples and keep track of the "losers"

    The first elements of each tuple are expected to be comparable, while the
    second elements are expected to be lists holding elements that "lost" in
    previous comparisons.

    The loser of this comparison will be attached to the list of losers for the
    winner of this comparison.

    Args:
        left_tup: Tuple of element to be compared and list of "losers",
            e.g., (1, [3, 4]).
        right_tup: Tuple of element to be compared and list of "losers",
            e.g., (2, [5, 6]).

    Returns:
        The "winner" of this comparison and its amended list of losers.
    """
    if left_tup == right_tup:
        return left_tup

    cand_left, losers_left = left_tup
    cand_right, losers_right = right_tup

    if cand_left < cand_right:
        winner = cand_left

        # Make sure input list doesn't get affected.
        losers = list(losers_left)
        losers.append(cand_right)
    else:
        winner = cand_right

        # Make sure input list doesn't get affected.
        losers = list(losers_right)
        losers.append(cand_left)

    return winner, losers


def test_compare(tup_left, tup_right, expected):
    """Test the compare method"""
    winner, losers = compare(tup_left, tup_right)

    assert (winner, losers) == expected, (
        f"Expected '{expected}' from 'compare({tup_left}, {tup_right})' "
        f"but got '({winner}, {losers})'."
    )


def test_find_smallest(array, expected):
    """Test the find_smallest method"""
    winners, n_comp = find_smallest(array=array)

    assert winners == expected[0], (
        f"Expected '{expected[0]}' when calling "
        f"'find_smallest(array={array})', but got '{winners}'."
    )

    assert n_comp == expected[1], (
        f"Expected {expected[1]} comparison(s) when calling "
        f"'find_smallest(array={array})', but got '{n_comp}'."
    )


def test_find_minimums(array, expected_mins, expected_n_comp):
    """Test the find_minimums method"""
    expected_min_1 = expected_mins[0]
    expected_min_2 = expected_mins[1]

    min_1, min_2, n_comp = find_minimums(array=array)

    assert min_1 == expected_min_1, (
        f"Expected {expected_min_1} when calling "
        f"'find_minimums(array={array})', but got {min_1}."
    )

    assert min_2 == expected_min_2, (
        f"Expected {expected_min_2} when calling "
        f"'find_minimums(array={array})', but got {min_2}."
    )

    assert n_comp <= expected_n_comp, (
        f"Expected {expected_n_comp} comparisons when calling "
        f"'find_minimums(array={array})', but got {n_comp}."
    )


if __name__ == '__main__':
    main()
