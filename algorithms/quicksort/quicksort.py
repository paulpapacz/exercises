#!/usr/bin/env python3

"""Implementation of a simple (recursive) quicksort algorithm."""

# Each element has the form:
# (<array>, <expected index of pivot element>)
TEST_ARRAYS_PIVOTS = (
    # One-element arrays.
    ([1], 0),
    ([0], 0),

    # Two-element arrays.
    ([1, 0], 0),
    ([0, 1], 1),

    # All elements have identical values.
    ([0, 0], 1),
    ([0, 0, 0], 2),

    # Some elements have identical values.
    ([1, 1, 0], 0),
    ([1, 0, 0], 1),
    ([2, 1, 0, 1], 2),
    ([2, 0, 0, 1], 2),
    ([2, 2, 0, 1], 1),

    # Ordered and unordered sequences.
    ([5, 4, 3, 2, 1], 0),
    ([2, 3, 4, 5, 1], 0),
    ([1, 2, 3, 4, 5], 4),
    ([4, 3, 2, 1, 5], 4),

    # Same set of values with different orderings but same pivot value.
    ([1, 2, 3, 4, 6, 7, 8, 5], 4),
    ([8, 7, 6, 4, 3, 2, 1, 5], 4),
    ([1, 8, 3, 7, 6, 4, 2, 5], 4),
)

# Each elements has the form:
# (<unsorted array>, <expected sorted array>)
TEST_ARRAYS_SORTED = (
    # Empty array.
    ([], []),

    # One-element arrays.
    ([0], [0]),
    ([1], [1]),

    # Two-element arrays.
    ([0, 1], [0, 1]),
    ([1, 0], [0, 1]),

    # Element values in decreasing order (with/without duplicates).
    ([5, 4, 3, 2, 1], [1, 2, 3, 4, 5]),
    ([5, 4, 3, 2, 2, 2, 1, 1], [1, 1, 2, 2, 2, 3, 4, 5]),

    # Element values in increasing order.
    ([1, 2, 3, 4, 5], [1, 2, 3, 4, 5]),
    ([1, 2, 3, 3, 4, 5, 5, 5], [1, 2, 3, 3, 4, 5, 5, 5]),

    # All elements have identical values.
    ([0, 0], [0, 0]),
    ([1, 1], [1, 1]),
    ([0, 0, 0], [0, 0, 0]),
    ([1, 1, 1], [1, 1, 1]),

    # Some elements have identical values.
    ([0, 1, 1, 1], [0, 1, 1, 1]),
    ([1, 1, 0, 1], [0, 1, 1, 1]),
    ([0, 1, 2, 3, 3], [0, 1, 2, 3, 3]),
    ([3, 3, 2, 1, 0], [0, 1, 2, 3, 3]),

    # All permutations of three distinct element values.
    ([0, 1, 2], [0, 1, 2]),
    ([0, 2, 1], [0, 1, 2]),
    ([1, 0, 2], [0, 1, 2]),
    ([1, 2, 0], [0, 1, 2]),
    ([2, 0, 1], [0, 1, 2]),
    ([2, 1, 0], [0, 1, 2]),

    # Elements in increasing/decreasing order with duplicates.
    ([2, 2, 4, 8, 16], [2, 2, 4, 8, 16]),
    ([16, 4, 8, 2, 2], [2, 2, 4, 8, 16]),
)


def main():
    for arr, piv in TEST_ARRAYS_PIVOTS:
        test_partition(arr, piv)

    for arr, sor in TEST_ARRAYS_SORTED:
        test_quicksort(arr, sor)


def quicksort(array, first=None, last=None):
    if not array:
        return

    if first is None:
        first = 0

    if last is None:
        last = len(array) - 1

    if first < last:
        pivot = partition(array, first=first, last=last)

        quicksort(array, first=first, last=pivot - 1)
        quicksort(array, first=pivot + 1, last=last)


def partition(array, first, last):
    pivot_value = array[last]

    i = first - 1
    for j in range(first, last):
        if array[j] <= pivot_value:
            i += 1
            array[i], array[j] = array[j], array[i]

    i += 1
    array[i], array[last] = array[last], array[i]

    return i


def test_partition(array, pivot):
    val = partition(array, 0, len(array) - 1)

    assert val == pivot, f"Got pivot value {val} but expected {pivot}."


def test_quicksort(array, sorted_array):
    quicksort(array)

    assert array == sorted_array, (
        f"Expected sorted array {sorted_array} but got {array} instead."
    )


if __name__ == '__main__':
    main()
