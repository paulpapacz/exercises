#!/usr/bin/env python3

"""Recursive quicksort taking equal pivot values into account."""

# Each element has the form:
# (<array>, <expected index of first pivot element>, <expected index of last pivot element>)
TEST_ARRAYS_PIVOTS = (
    # One-element arrays.
    ([1], 0, 0),
    ([0], 0, 0),

    # Two-element arrays.
    ([1, 0], 0, 0),
    ([0, 1], 1, 1),

    # All elements have identical values.
    ([0, 0], 0, 1),
    ([0, 0, 0], 0, 2),

    # Some elements have identical values.
    ([1, 1, 0], 0, 0),
    ([1, 0, 0], 0, 1),
    ([2, 1, 0, 1], 1, 2),
    ([2, 0, 0, 1], 2, 2),
    ([2, 2, 0, 1], 1, 1),

    # Ordered and unordered sequences, all values distinct.
    ([5, 4, 3, 2, 1], 0, 0),
    ([2, 3, 4, 5, 1], 0, 0),
    ([1, 2, 3, 4, 5], 4, 4),
    ([4, 3, 2, 1, 5], 4, 4),

    # Same sets of values in different orders but same pivot value and
    # duplicate pivot values.
    ([1, 2, 3, 4, 5, 6, 7, 8, 5], 4, 5),
    ([8, 7, 6, 5, 4, 3, 2, 1, 5], 4, 5),
    ([1, 8, 3, 7, 6, 4, 2, 5, 5], 4, 5),
    ([1, 2, 3, 4, 5, 5, 6, 7, 8, 5], 4, 6),
    ([8, 7, 6, 5, 5, 4, 3, 2, 1, 5], 4, 6),
    ([5, 1, 8, 3, 7, 6, 4, 2, 5, 5], 4, 6),

    # Same sets of values in different orders but same pivot value and
    # duplicate pivot and other values.
    ([1, 1, 2, 3, 5, 5, 6, 5], 4, 6),
    ([2, 3, 1, 1, 5, 5, 6, 5], 4, 6),
    ([6, 3, 1, 1, 5, 5, 2, 5], 4, 6),
    ([5, 5, 1, 1, 3, 6, 2, 5], 4, 6),
)

# Each element has the form:
# (<unsorted array>, <expected sorted array>)
TEST_ARRAYS_SORTED = (
    # Empty array.
    ([], []),

    # One-element arrays.
    ([0], [0]),
    ([1], [1]),

    # Two-element arrays.
    ([0, 1], [0, 1]),
    ([1, 0], [0, 1]),

    # Element values in decreasing order (with/without duplicates).
    ([5, 4, 3, 2, 1], [1, 2, 3, 4, 5]),
    ([5, 4, 3, 2, 2, 2, 1, 1], [1, 1, 2, 2, 2, 3, 4, 5]),

    # Element values in increasing order.
    ([1, 2, 3, 4, 5], [1, 2, 3, 4, 5]),
    ([1, 2, 3, 3, 4, 5, 5, 5], [1, 2, 3, 3, 4, 5, 5, 5]),

    # All elements have identical values.
    ([0, 0], [0, 0]),
    ([1, 1], [1, 1]),
    ([0, 0, 0], [0, 0, 0]),
    ([1, 1, 1], [1, 1, 1]),

    # Some elements have identical values.
    ([0, 1, 1, 1], [0, 1, 1, 1]),
    ([1, 1, 0, 1], [0, 1, 1, 1]),
    ([0, 1, 2, 3, 3], [0, 1, 2, 3, 3]),
    ([3, 3, 2, 1, 0], [0, 1, 2, 3, 3]),

    # All permutations of three distinct element values.
    ([0, 1, 2], [0, 1, 2]),
    ([0, 2, 1], [0, 1, 2]),
    ([1, 0, 2], [0, 1, 2]),
    ([1, 2, 0], [0, 1, 2]),
    ([2, 0, 1], [0, 1, 2]),
    ([2, 1, 0], [0, 1, 2]),

    # Elements in increasing/decreasing order with duplicates.
    ([2, 2, 4, 8, 16], [2, 2, 4, 8, 16]),
    ([16, 4, 8, 2, 2], [2, 2, 4, 8, 16]),
)


def main():
    for arr, pivot_first, pivot_last in TEST_ARRAYS_PIVOTS:
        test_partition_equal(arr, pivot_first, pivot_last)

    for arr, sor in TEST_ARRAYS_SORTED:
        test_quicksort_equal(arr, sor)


def partition_equal(array, first, last):
    pivot_value = array[last]

    i = first - 1
    for j in range(first, last):
        if array[j] < pivot_value:
            i += 1
            array[i], array[j] = array[j], array[i]

    i += 1
    array[i], array[last] = array[last], array[i]

    pivot_first = i

    for k in range(i + 1, last + 1):
        if array[k] == pivot_value:
            i += 1
            array[k], array[i] = array[i], array[k]

    return pivot_first, i


def quicksort_equal(array, first=None, last=None):
    if not array:
        return

    if first is None:
        first = 0

    if last is None:
        last = len(array) - 1

    if first < last:
        equal_first, equal_last = partition_equal(array, first, last)

        quicksort_equal(array, first=first, last=equal_first - 1)
        quicksort_equal(array, first=equal_last + 1, last=last)


def test_partition_equal(array, pivot_first, pivot_last):
    p_first, p_last = partition_equal(array, 0, len(array) - 1)

    assert p_first == pivot_first, (
        f"Got first pivot value {p_first} but expected {pivot_first}."
    )

    assert p_last == pivot_last, (
        f"Got first pivot value {p_last} but expected {pivot_last}."
    )


def test_quicksort_equal(array, sorted_array):
    quicksort_equal(array)

    assert array == sorted_array, (
        f"Expected sorted array {sorted_array} but got {array} instead."
    )


if __name__ == '__main__':
    main()
