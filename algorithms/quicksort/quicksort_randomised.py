#!/usr/bin/env python3

"""Randomised version of quicksort.

Same as quicksort with the addition of a random swap of the pivot element
before partitioning.
"""

import random

from quicksort import partition
from quicksort import TEST_ARRAYS_SORTED


def main():
    for arr, sor in TEST_ARRAYS_SORTED:
        test_quicksort_randomised(arr, sor)


def quicksort_randomised(array, first=None, last=None):
    if not array:
        return

    if first is None:
        first = 0

    if last is None:
        last = len(array) - 1

    if first < last:
        rand = random.randint(first, last)

        array[last], array[rand] = array[rand], array[last]

        pivot = partition(array, first, last)

        quicksort_randomised(array, first=first, last=pivot - 1)
        quicksort_randomised(array, first=pivot + 1, last=last)


def test_quicksort_randomised(array, array_sorted):
    quicksort_randomised(array)

    assert array == array_sorted, (
        f"Expected sorted array {array_sorted} but got {array} instead."
    )


if __name__ == '__main__':
    main()
