#!/usr/bin/env python3

"""
Implementation of a uniform random number (integer) generator for a range [a,b]

The implementation only makes calls to RANDOM(0, 1).
Assuming a > 0, b > a
"""

import math
import random


def main():
    start = 0
    stop = 9

    for _ in range(10000):
        assert start <= rand(start, stop) <= stop

    start = 5
    stop = 10

    for _ in range(10000):
        assert start <= rand(start, stop) <= stop

    start = 9
    stop = 1000

    for _ in range(10000):
        assert start <= rand(start, stop) <= stop


def rand(start, stop):
    rng = stop - start
    n_bits = math.ceil(math.log2(rng))
    res = None

    while res is None or res > rng:
        res = 0
        for i in range(n_bits):
            rnd = random.randint(0, 1)
            rnd <<= i

            res += rnd

    return res + start


if __name__ == '__main__':
    main()
