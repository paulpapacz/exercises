import pytest


@pytest.fixture
def test_tree_empty():
    return {
        'root': -1,
        'key': [],
        'left': [],
        'right': [],
        'parent': [],
    }


@pytest.fixture
def test_tree_one_node():
    tree = {
        'root': 0,
        'key': [9, ],
        'left': [None, ],
        'right': [None, ],
        'parent': [None, ],
    }

    return tree, '9\n'


@pytest.fixture
def test_tree_two_nodes():
    tree = {
        'root': 0,
        'key': [9, 1, ],
        'left': [1, None, ],
        'right': [None, None, ],
        'parent': [None, 0, ],
    }

    return tree, '9\n1\n'


@pytest.fixture
def test_tree_three_nodes_l_r():
    tree = {
        'root': 0,
        'key': [9, 1, 2, ],
        'left': [1, None, None, ],
        'right': [2, None, None, ],
        'parent': [None, 0, 0, ],
    }

    # Different order expected for the different algorithms.
    return tree, '9\n1\n2\n', '9\n2\n1\n'


@pytest.fixture
def test_tree_three_nodes_r_l():
    tree = {
        'root': 0,
        'key': [9, 1, 2, ],
        'left': [2, None, None, ],
        'right': [1, None, None, ],
        'parent': [None, 0, 0, ],
    }

    # Different order expected for the different algorithms.
    return tree, '9\n2\n1\n', '9\n1\n2\n'


@pytest.fixture
def test_tree_three_nodes_l_l():
    tree = {
        'root': 0,
        'key': [9, 1, 2, ],
        'left': [1, 2, None, ],
        'right': [None, None, None, ],
        'parent': [None, 0, 1, ],
    }

    # Potentially different order expected for the different algorithms.
    return tree, '9\n1\n2\n', '9\n1\n2\n'


@pytest.fixture
def test_tree_three_nodes_r_r():
    tree = {
        'root': 0,
        'key': [9, 1, 2, ],
        'left': [None, None, None, ],
        'right': [1, 2, None, ],
        'parent': [None, 0, 1, ],
    }

    # Potentially different order expected for the different algorithms.
    return tree, '9\n1\n2\n', '9\n1\n2\n'


@pytest.fixture
def test_tree_three_nodes_l_r_garbage_l():
    # Tree contains two garbage entries, left of the root node.
    tree = {
        'root': 2,
        'key': [10, 11, 9, 1, 2, ],
        'left': [1, 2, 3, None, None, ],
        'right': [None, None, 4, None, None, ],
        'parent': [None, None, None, 2, 2, ],
    }

    # Different order expected for the different algorithms.
    return tree, '9\n1\n2\n', '9\n2\n1\n'


@pytest.fixture
def test_tree_three_nodes_l_r_garbage_r():
    # Tree contains two garbage entries, right of the rightmost node.
    tree = {
        'root': 0,
        'key': [9, 1, 2, 12, 13, ],
        'left': [1, None, None, None, None, ],
        'right': [2, None, None, 4, 2, ],
        'parent': [None, 0, 0, None, None, ]
    }

    # Different order expected for the different algorithms.
    return tree, '9\n1\n2\n', '9\n2\n1\n'


@pytest.fixture
def test_tree_three_nodes_l_r_garbage_m():
    # Tree contains two garbage entries, between the valid nodes.
    tree = {
        'root': 0,
        'key': [9, 12, 1, 13, 2, ],
        'left': [2, None, None, None, None, ],
        'right': [4, None, None, None, None, ],
        'parent': [None, None, 0, None, 0, ]
    }

    # Different order expected for the different algorithms.
    return tree, '9\n1\n2\n', '9\n2\n1\n'


@pytest.fixture
def test_tree_ten_nodes():
    tree = {
        'root': 5,
        # i:       0   1     2     3   4     5     6     7   8     9
        # garbage:     X                                 X
        'key':    [12, 15,   4,    10, 2,    18,   7,    14, 21,   5, ],
        'left':   [6,  7,    9,    4,  None, 0,    None, 5,  None, None, ],
        'right':  [2,  None, None, 8,  None, 3,    None, 1,  None, None, ],
        'parent': [5,  None, 0,    5,  3,    None, 0,    None, 3,    2, ],
    }

    # Different order expected for the different algorithms.
    return tree, '18\n12\n7\n4\n5\n10\n2\n21\n', '18\n10\n21\n2\n12\n4\n5\n7\n'
