#!/usr/bin/env python3


def main():
    # Using a dict containing three lists to represent the rooted (binary) tree
    tree = {
        'root': 5,
        # i:       0   1     2     3   4     5     6     7     8     9
        # garbage:     X                                 X
        'key':    [12, 15,   4,    10, 2,    18,   7,    14,   21,   5, ],
        'left':   [6,  7,    9,    4,  None, 0,    None, 5,    None, None, ],
        'right':  [2,  None, None, 8,  None, 3,    None, 1,    None, None, ],
        'parent': [5,  None, 0,    5,  3,    None, 0,    None, 3,    2, ],
    }

    print_tree(tree)

    print('print keys recursive:')
    print_keys_recursive(tree)

    print('print keys nonrecursice:')
    print_keys_nonrecursive(tree)

    print('print keys nonrecursice no stack:')
    print_keys_nonrecursive_no_stack(tree)


def print_tree(tree):
    print(f"root: {tree['root']}")
    width = 5
    keys = [
        '{0: >{width}}'.format(str(key), width=width) for key in tree['key']
    ]
    lefts = [
        '{0: >{width}}'.format(str(left), width=width) for left in tree['left']
    ]
    rights = [
        '{0: >{width}}'.format(str(right), width=width)
        for right in tree['right']
    ]
    print('key:  ', ','.join(keys))
    print('left: ', ','.join(lefts))
    print('right:', ','.join(rights))


def print_keys_recursive(tree, node=-1):
    root = tree['root']

    if root < 0:
        # Empty tree.
        return

    if node == -1:
        node = root

    if node is not None:
        print(tree['key'][node])

        print_keys_recursive(tree, node=tree['left'][node])
        print_keys_recursive(tree, node=tree['right'][node])


def print_keys_nonrecursive(tree):
    node = tree['root']

    if node < 0:
        # Empty tree.
        return

    stack = [None]
    while node is not None:
        print(tree['key'][node])

        left = tree['left'][node]
        right = tree['right'][node]

        if left is not None:
            stack.append(left)

        if right is not None:
            stack.append(right)

        node = stack.pop()


def print_keys_nonrecursive_no_stack(tree):
    node = tree['root']

    if node < 0:
        # Empty tree.
        return

    key = tree['key']
    left = tree['left']
    right = tree['right']
    parent = tree['parent']

    prev = parent[node]

    while node is not None:
        if parent[node] == prev:
            print(key[node])
            prev = node

            if left[node] is None:
                node = parent[node]
            else:
                node = left[node]

        elif left[node] == prev:
            prev = node

            if right[node] is None:
                node = parent[node]
            else:
                node = right[node]

        else:
            prev = node
            node = parent[node]


if __name__ == '__main__':
    main()
