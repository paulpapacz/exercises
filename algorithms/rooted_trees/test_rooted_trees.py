from .rooted_trees import print_keys_recursive
from .rooted_trees import print_keys_nonrecursive
from .rooted_trees import print_keys_nonrecursive_no_stack


# Test print_keys_recursive:

def test_print_keys_recursive_empty(capfd, test_tree_empty):
    print_keys_recursive(test_tree_empty)
    captured = capfd.readouterr()

    assert captured.out == ''


def test_print_keys_recursive_one_node(capfd, test_tree_one_node):
    print_keys_recursive(test_tree_one_node[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_one_node[1]


def test_print_keys_recursive_two_node(capfd, test_tree_two_nodes):
    print_keys_recursive(test_tree_two_nodes[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_two_nodes[1]


def test_print_keys_recursive_three_nodes_l_r(
        capfd, test_tree_three_nodes_l_r):
    print_keys_recursive(test_tree_three_nodes_l_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r[1]


def test_print_keys_recursive_three_nodes_r_l(
        capfd, test_tree_three_nodes_r_l):
    print_keys_recursive(test_tree_three_nodes_r_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_r_l[1]


def test_print_keys_recursive_three_nodes_r_l_garbage_l(
        capfd, test_tree_three_nodes_l_r_garbage_l):
    print_keys_recursive(test_tree_three_nodes_l_r_garbage_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_l[1]


def test_print_keys_recursive_three_nodes_r_l_garbage_r(
        capfd, test_tree_three_nodes_l_r_garbage_r):
    print_keys_recursive(test_tree_three_nodes_l_r_garbage_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_r[1]


def test_print_keys_recursive_three_nodes_r_l_garbage_m(
        capfd, test_tree_three_nodes_l_r_garbage_m):
    print_keys_recursive(test_tree_three_nodes_l_r_garbage_m[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_m[1]


def test_print_keys_recursive_ten_nodes(capfd, test_tree_ten_nodes):
    print_keys_recursive(test_tree_ten_nodes[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_ten_nodes[1]


def test_print_keys_recursive_three_nodes_l_l(
        capfd, test_tree_three_nodes_l_l):
    print_keys_recursive(test_tree_three_nodes_l_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_l[1]


def test_print_keys_recursive_three_nodes_r_r(
        capfd, test_tree_three_nodes_r_r):
    print_keys_recursive(test_tree_three_nodes_r_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_r_r[1]


# Test print_keys_nonrecursive:

def test_print_keys_nonrecursive_empty(capfd, test_tree_empty):
    print_keys_nonrecursive(test_tree_empty)
    captured = capfd.readouterr()

    assert captured.out == ''


def test_print_keys_nonrecursive_one_node(capfd, test_tree_one_node):
    print_keys_nonrecursive(test_tree_one_node[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_one_node[1]


def test_print_keys_nonrecursive_two_node(capfd, test_tree_two_nodes):
    print_keys_nonrecursive(test_tree_two_nodes[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_two_nodes[1]


def test_print_keys_nonrecursive_three_nodes_l_r(
        capfd, test_tree_three_nodes_l_r):
    print_keys_nonrecursive(test_tree_three_nodes_l_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r[2]


def test_print_keys_nonrecursive_three_nodes_l_l(
        capfd, test_tree_three_nodes_l_l):
    print_keys_nonrecursive(test_tree_three_nodes_l_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_l[1]


def test_print_keys_nonrecursive_three_nodes_r_r(
        capfd, test_tree_three_nodes_r_r):
    print_keys_nonrecursive(test_tree_three_nodes_r_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_r_r[1]


def test_print_keys_nonrecursive_three_nodes_r_l(
        capfd, test_tree_three_nodes_r_l):
    print_keys_nonrecursive(test_tree_three_nodes_r_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_r_l[2]


def test_print_keys_nonrecursive_three_nodes_r_l_garbage_l(
        capfd, test_tree_three_nodes_l_r_garbage_l):
    print_keys_nonrecursive(test_tree_three_nodes_l_r_garbage_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_l[2]


def test_print_keys_nonrecursive_three_nodes_r_l_garbage_r(
        capfd, test_tree_three_nodes_l_r_garbage_r):
    print_keys_nonrecursive(test_tree_three_nodes_l_r_garbage_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_r[2]


def test_print_keys_nonrecursive_three_nodes_r_l_garbage_m(
        capfd, test_tree_three_nodes_l_r_garbage_m):
    print_keys_nonrecursive(test_tree_three_nodes_l_r_garbage_m[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_m[2]


def test_print_keys_nonrecursive_ten_nodes(capfd, test_tree_ten_nodes):
    print_keys_nonrecursive(test_tree_ten_nodes[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_ten_nodes[2]


# Test print_keys_nonrecursive_no_stack:

def test_print_keys_nonrecursive_no_stack_empty(capfd, test_tree_empty):
    print_keys_nonrecursive_no_stack(test_tree_empty)
    captured = capfd.readouterr()

    assert captured.out == ''


def test_print_keys_nonrecursive_no_stack_one_node(capfd, test_tree_one_node):
    print_keys_nonrecursive_no_stack(test_tree_one_node[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_one_node[1]


def test_print_keys_nonrecursive_no_stack_two_node(capfd, test_tree_two_nodes):
    print_keys_nonrecursive_no_stack(test_tree_two_nodes[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_two_nodes[1]


def test_print_keys_nonrecursive_no_stack_three_nodes_l_r(
        capfd, test_tree_three_nodes_l_r):
    print_keys_nonrecursive_no_stack(test_tree_three_nodes_l_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r[1]


def test_print_keys_nonrecursive_no_stack_three_nodes_l_l(
        capfd, test_tree_three_nodes_l_l):
    print_keys_nonrecursive_no_stack(test_tree_three_nodes_l_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_l[1]


# Currently having child nodes to the right only is not supported by this
# algorithm.
#
#def test_print_keys_nonrecursive_no_stack_three_nodes_r_r(
        #capfd, test_tree_three_nodes_r_r):
    #print_keys_nonrecursive_no_stack(test_tree_three_nodes_r_r[0])
    #captured = capfd.readouterr()

    #assert captured.out == test_tree_three_nodes_r_r[1]


def test_print_keys_nonrecursive_no_stack_three_nodes_r_l(
        capfd, test_tree_three_nodes_r_l):
    print_keys_nonrecursive_no_stack(test_tree_three_nodes_r_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_r_l[1]


def test_print_keys_nonrecursive_no_stack_three_nodes_r_l_garbage_l(
        capfd, test_tree_three_nodes_l_r_garbage_l):
    print_keys_nonrecursive_no_stack(test_tree_three_nodes_l_r_garbage_l[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_l[1]


def test_print_keys_nonrecursive_no_stack_three_nodes_r_l_garbage_r(
        capfd, test_tree_three_nodes_l_r_garbage_r):
    print_keys_nonrecursive_no_stack(test_tree_three_nodes_l_r_garbage_r[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_r[1]


def test_print_keys_nonrecursive_no_stack_three_nodes_r_l_garbage_m(
        capfd, test_tree_three_nodes_l_r_garbage_m):
    print_keys_nonrecursive_no_stack(test_tree_three_nodes_l_r_garbage_m[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_three_nodes_l_r_garbage_m[1]


def test_print_keys_nonrecursive_no_stack_ten_nodes(
        capfd, test_tree_ten_nodes):
    print_keys_nonrecursive_no_stack(test_tree_ten_nodes[0])
    captured = capfd.readouterr()

    assert captured.out == test_tree_ten_nodes[1]
