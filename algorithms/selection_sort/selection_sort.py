#!/usr/bin/env python3

"""Simple implementation of the Selection Sort algorithm

Implements an algorithm that sorts a given array (Python :obj:`list`) by
iteratively finding the smallest element in the subarray A[i..n] and swapping
it with A[i], where `n = len(A)` and `i` starts at `0`.
"""


def main():
    """Entry point"""

    array = [
        6, 5, 7, 4, 3, 2, 1,
    ]

    sort(array)

    print(f'Sorted array: {array}')


def sort(array):
    """Selection Sort

    Sorts given list of integers in place.

    Args:
        array (:obj:`list` of :obj:`int`): List of integers to be sorted.
    """
    len_array = len(array)

    for i, _ in enumerate(array):
        # Don't need the last iteration.
        if i >= len_array - 1:
            break

        j_min = i
        for j, a_y in enumerate(array):
            # Only compare the array elements to the right.
            if j <= i:
                continue

            if a_y < array[j_min]:
                j_min = j

        array[i], array[j_min] = array[j_min], array[i]


if __name__ == '__main__':
    main()
