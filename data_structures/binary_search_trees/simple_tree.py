#!/usr/bin/env python3

"""Simple Binary Search Tree

Representing the following binary search tree using arrays:

                    10
                  ´    `
                ´        `
              4           17
            ´   `       ´    `
           ´     `     ´      `
          1       5   16      21
           `                 ´
            `               ´
             2            19

Nodes and their properties are represented by indices into the respective
arrays.
"""

# pylint: disable=bad-whitespace
# node =     0     1     2     3     4     5     6     7     8

KEY    = [  10,    4,   17,    1,    5,   16,   21,    2,   19]

PARENT = [None,    0,    0,    1,    1,    2,    2,    3,    6]

LEFT   = [   1,    3,    5, None, None, None,    8, None, None]

RIGHT  = [   2,    4,    6,    7, None, None, None, None, None]
# pylint: enable=bad-whitespace


def inorder_print_rec(node: int = 0):
    """Recursive function to print keys in ascending order

    Args:
        node: Integer representing a tree node.
    """
    if node is not None:
        inorder_print_rec(LEFT[node])
        print(KEY[node])
        inorder_print_rec(RIGHT[node])


inorder_print_rec()
print()


def inorder_print_stack():
    """Print keys in ascending order

    Uses a stack as an auxiliary data structure
    """
    stack = []
    curr = 0

    while True:
        if curr is not None:
            # Traverse sub-tree to the left until we hit a dead end.
            stack.append(curr)
            curr = LEFT[curr]
            continue

        if len(stack) > 0:
            # Reached the leftmost element in the sub-tree.
            # Print the key and proceed with element to the right.
            curr = stack.pop()
            print(KEY[curr])
            curr = RIGHT[curr]
        else:
            # Empty stack means all elements have been visited.
            break


inorder_print_stack()
print()


def inorder_print():
    """Non-recursive function to print keys in ascending order"""
    curr = 0
    prev = None

    while curr is not None:
        if prev == RIGHT[curr]:
            prev = curr
            curr = PARENT[curr]
            continue

        if LEFT[curr] is not None and LEFT[curr] != prev:
            prev = curr
            curr = LEFT[curr]
            continue

        print(KEY[curr])

        if RIGHT[curr] is not None:
            prev = curr
            curr = RIGHT[curr]
            continue

        prev = curr
        curr = PARENT[curr]


inorder_print()
print()


def maximum_rec(node: int = 0) -> int:
    """Recursively find node containing maximum value in sub-tree

    Args:
        node: Integer identifying root node of the sub-tree.

    Returns:
        Integer identifying node with maximum value in the sub-tree.
    """
    if RIGHT[node] is not None:
        return maximum_rec(RIGHT[node])
    return node


assert maximum_rec() == 6


def maximum(node: int = 0):
    """Iteratively find node containing maximum value in sub-tree

    Args:
        node: Integer identifying root node of the sub-tree.

    Returns:
        Integer identifying node with maximum value in the sub-tree.
    """
    while RIGHT[node] is not None:
        node = RIGHT[node]
    return node


assert maximum() == 6


def minimum_rec(node: int = 0):
    """Recursively find node containing minimum value in sub-tree

    Args:
        node: Integer identifying root node of the sub-tree.

    Returns:
        Integer identifying node with minimum value in the sub-tree.
    """
    if LEFT[node] is not None:
        return minimum_rec(LEFT[node])
    return node


assert minimum_rec() == 3


def minimum(node: int = 0):
    """Iteratively find node containing minimum value in sub-tree

    Args:
        node: Integer identifying root node of the sub-tree.

    Returns:
        Integer identifying node with minimum value in the sub-tree.
    """
    while LEFT[node] is not None:
        node = LEFT[node]

    return node


assert minimum() == 3


def search(s_key, node: int = 0) -> int:
    """Iteratively search for given value in given sub-tree

    Args:
        s_key: Key to be searched for in sub-tree.
        node: Integer identifying root node of sub-tree to search in.

    Returns:
        Integer identifying node with the search key or None if not found.
    """
    while node is not None and KEY[node] != s_key:
        if s_key < KEY[node]:
            node = LEFT[node]
        elif s_key > KEY[node]:
            node = RIGHT[node]

    return node


assert search(0) is None
assert search(1) == 3
assert search(10) == 0
assert search(19) == 8
assert search(21) == 6
assert search(22) is None


def search_rec(s_key, node: int = 0) -> int:
    """Recursively search for given value in given sub-tree

    Args:
        s_key: Key to be searched for in sub-tree.
        node: Integer identifying root node of sub-tree to search in.

    Returns:
        Integer identifying node with the search key or None if not found.
    """
    if node is None or s_key == KEY[node]:
        return node

    if s_key < KEY[node]:
        return search_rec(s_key, LEFT[node])

    return search_rec(s_key, RIGHT[node])


assert search_rec(0) is None
assert search_rec(1) == 3
assert search_rec(10) == 0
assert search_rec(19) == 8
assert search_rec(21) == 6
assert search_rec(22) is None


def successor(node: int) -> int:
    """Find successor of given node

    Args:
        node: Integer identifying node whose successor is to be found.

    Returns:
        Integer identifying the node's successor.
    """
    if RIGHT[node] is not None:
        return minimum(RIGHT[node])

    curr = node
    parent = PARENT[node]
    while parent is not None and RIGHT[parent] == curr:
        curr = parent
        parent = PARENT[curr]

    return parent


assert successor(0) == 5
assert successor(4) == 0
assert successor(6) is None
assert successor(8) == 6


def predecessor(node: int) -> int:
    """Find predecessor of given node

    Args:
        node: Integer identifying node whose predecessor is to be found.

    Returns:
        Integer identifying the node's predecessor.
    """
    if LEFT[node] is not None:
        return maximum(LEFT[node])

    curr = node
    parent = PARENT[node]
    while parent is not None and LEFT[parent] == curr:
        curr = parent
        parent = PARENT[curr]

    return parent


assert predecessor(0) == 4
assert predecessor(3) is None
assert predecessor(4) == 1
assert predecessor(8) == 2
assert predecessor(6) == 8
