#!/usr/bin/env python3

"""
Simple implementation of circular queue/buffer using a fixed-length array/list.

Deliberately only using basic Python data structures. The queue is represented
by a dict, consisting of one array (i.e. Python list) and two indices
indicating the head and the tail of the queue, repectively.

The queue is considered empty when head equals the tail, and full when the
difference between the head and the tail modulo length equals 1.

The total length of the queue is stored as `len` but could as well be derived
from the fixed-length list.
"""


def main():
    test_queue()


def init_queue(length):
    return dict({
        'len': length,
        'elements': [None] * length,
        'head': 0,
        'tail': 0,
    })


def is_empty(queue):
    return queue['tail'] == queue['head']


def is_full(queue):
    return (queue['head'] - queue['tail']) % queue['len'] == 1


def enqueue(queue, value):
    if is_full(queue):
        print(f"WARNING: Cannot insert value {value}, queue is full!")
        return

    tail = queue['tail']
    queue['elements'][tail] = value
    queue['tail'] = (tail + 1) % queue['len']


def dequeue(queue):
    if is_empty(queue):
        print(f"WARNING: Cannot dequeue, queue is empty!")
        return None

    head = queue['head']
    value = queue['elements'][head]
    queue['head'] = (head + 1) % queue['len']

    return value


def test_queue():
    queue = init_queue(length=6)

    display_queue(queue)

    assert is_empty(queue), f"Queue not empty but should be!"
    assert not is_full(queue), f"Queue full but shouldn't be!"

    # Queue is empty, dequeue shouldn't change that.
    dequeue(queue)

    assert is_empty(queue), f"Queue not empty but should be!"
    assert not is_full(queue), f"Queue full but shouldn't be!"

    enqueue(queue, 9)

    assert not is_empty(queue), f"Queue empty but shouldn't be!"
    assert not is_full(queue), f"Queue full but shouldn't be!"

    enqueue(queue, 8)
    enqueue(queue, 7)
    enqueue(queue, 6)
    enqueue(queue, 5)

    display_queue(queue)

    assert not is_empty(queue), f"Queue empty but shouldn't be!"
    assert is_full(queue), f"Queue not full but should be!"

    # Queue is full, inserting another element shouldn't change that.
    enqueue(queue, 5)

    assert not is_empty(queue), f"Queue empty but shouldn't be!"
    assert is_full(queue), f"Queue not full but should be!"

    val = dequeue(queue)
    expected = 9

    assert val == expected, f"Got {val} but expected {expected}!"

    # Dequeued one element, queue shouldn't be full nor empty:

    assert not is_empty(queue), f"Queue empty but shouldn't be!"
    assert not is_full(queue), f"Queue full but shouldn't be!"

    dequeue(queue)
    dequeue(queue)

    display_queue(queue)

    dequeue(queue)
    val = dequeue(queue)
    expected = 5

    assert val == expected, f"Got {val} but expected {expected}!"

    # Queue should be empty now.
    assert is_empty(queue), f"Queue not empty but should be!"
    assert not is_full(queue), f"Queue full but shouldn't be!"

    # Queue is empty, dequeue shouldn't change that.
    dequeue(queue)

    assert is_empty(queue), f"Queue not empty but should be!"
    assert not is_full(queue), f"Queue full but shouldn't be!"


def display_queue(queue):
    length = queue['len']
    head = queue['head']
    tail = queue['tail']
    elements = queue['elements']

    result = '---> Queue: '

    if is_empty(queue):
        result += '_ ' * length
        print(result)
        return

    # Iterate fixed-length list `elements` and display either the value of the
    # element or an underscore, depending on whether the element is part of the
    # queue or garbage data.
    for n_ele, ele in enumerate(elements):
        if head < tail:
            if head <= n_ele < tail:
                result += str(ele) + ' '
            else:
                result += '_ '
        elif tail < head:
            if not (tail <= n_ele < head):
                result += str(ele) + ' '
            else:
                result += '_ '

    print(result)


if __name__ == '__main__':
    main()
