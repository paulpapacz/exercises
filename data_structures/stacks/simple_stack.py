#!/usr/bin/env python3

"""
Simple implementation of a fixed-length stack.

Deliberately only using basic Python data structures. The stack is represented
by a dict, consisting of an array (i.e. Python list) and an index pointing to
the top of stack (TOS).

Elements are not removed from the array, the index `top` is keeping track of
the TOS instead.

The length of the stack is stored as `len` but could as well be derived from
the fixed-length list.
"""


def main():
    test_simple_stack()


def init_stack(length):
    return dict({
        'elements': [None] * length,
        'len': length,
        'top': 0,
    })


def is_empty(stack):
    return stack['top'] == 0


def is_full(stack):
    return stack['top'] == stack['len']


def push(stack, value):
    if is_full(stack):
        print(f"WARNING: Cannot push value {value}, stack is full!")
        return

    stack['elements'][stack['top']] = value
    stack['top'] += 1


def pop(stack):
    if is_empty(stack):
        print("WARNING: Cannot pop element, stack is empty!")
        return None

    stack['top'] -= 1
    return stack['elements'][stack['top']]


def test_simple_stack():
    stack = init_stack(length=10)

    print(f"stack: {stack}")

    assert is_empty(stack), "Stack not empty but should be!"
    assert not is_full(stack), "Stack full but shouldn't be!"

    push(stack, 1)

    assert not is_empty(stack), "Stack empty but shouldn't be!"
    assert not is_full(stack), "Stack full but shouldn't be!"

    val = pop(stack)
    expected = 1

    assert val == expected, f"Got {val} but expected {expected}!"
    assert is_empty(stack), "Stack not empty but should be!"
    assert not is_full(stack), "Stack full but shouldn't be!"

    val = pop(stack)
    expected = None

    assert val == expected, f"Got {val} but expected {expected}!"
    assert is_empty(stack), "Stack not empty but should be!"
    assert not is_full(stack), "Stack full but shouldn't be!"

    push(stack, 1)
    push(stack, 2)
    push(stack, 1)
    push(stack, 4)
    push(stack, 1)
    push(stack, 6)
    push(stack, 1)
    push(stack, 8)
    push(stack, 1)
    push(stack, 9)

    assert not is_empty(stack), "Stack empty but shouldn't be!"
    assert is_full(stack), "Stack not full but should be!"

    push(stack, 1)

    assert not is_empty(stack), "Stack empty but shouldn't be!"
    assert is_full(stack), "Stack not full but should be!"

    val = pop(stack)
    expected = 9

    assert val == expected, f"Got {val} but expected {expected}!"
    assert not is_empty(stack), "Stack empty but shouldn't be!"
    assert not is_full(stack), "Stack full but shouldn't be!"

    pop(stack)
    pop(stack)
    pop(stack)
    val = pop(stack)
    expected = 6

    assert val == expected, f"Got {val} but expected {expected}!"
    assert not is_empty(stack), "Stack empty but shouldn't be!"
    assert not is_full(stack), "Stack full but shouldn't be!"

    pop(stack)
    pop(stack)
    pop(stack)
    pop(stack)
    val = pop(stack)
    expected = 1

    assert val == expected, f"Got {val} but expected {expected}!"
    assert is_empty(stack), "Stack not empty but should be!"
    assert not is_full(stack), "Stack full but shouldn't be!"


if __name__ == '__main__':
    main()
