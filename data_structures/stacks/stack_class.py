#!/usr/bin/env python3

"""
Class-based implementation of a simple, fixed-length stack.

The class implements basic stack methods, as well as a method to retrieve the
currently largest value in the stack, in constant time.

The elements pushed to the stack are expected to be comparable.
"""


def main():
    test_stack_class()


class Stack:
    def __init__(self, length):
        self.top = 0
        self.length = length
        self.elements = [None] * length
        self.max_indices = []

    def is_empty(self):
        return self.top == 0

    def is_full(self):
        return self.top == self.length

    def push(self, value):
        if self.is_full():
            print(f"WARNING: Cannot push value {value}, stack is full!")
            return

        self.elements[self.top] = value

        if not self.max_indices:
            self.max_indices.append(self.top)
        elif value > self.elements[self.max_indices[-1]]:
            self.max_indices.append(self.top)

        self.top += 1

    def pop(self):
        if self.is_empty():
            print(f"WARNING: Cannot pop value, stack is empty!")
            return None

        self.top -= 1

        max_index = self.max_indices[-1]

        if max_index == self.top:
            self.max_indices.pop()

        val = self.elements[self.top]

        return val

    def max_value(self):
        if self.is_empty():
            print("WARNING: Empty stack has no max value!")
            return None

        return self.elements[self.max_indices[-1]]


def test_stack_class():
    stack = Stack(length=10)

    assert stack.is_empty(), "Stack not empty but should be!"
    assert not stack.is_full(), "Stack full but shouldn't be!"

    val = stack.max_value()
    assert val is None, f"Got {val} but no max value expected!"

    stack.push(2)
    stack.push(1)
    stack.push(2)

    assert not stack.is_empty(), "Stacknot empty but shouldn't be!"
    assert not stack.is_full(), "Stack full but shouldn't be!"

    val = stack.max_value()
    expected = 2

    assert val == expected, f"Got max value {val} but expected {expected}!"

    stack.push(3)
    stack.push(4)

    val = stack.max_value()
    expected = 4

    assert val == expected, f"Got max value {val} but expected {expected}!"

    val = stack.pop()
    expected = 4

    assert val == expected, f"Got {val} but expected {expected}!"

    val = stack.max_value()
    expected = 3

    assert val == expected, f"Got max value {val} but expected {expected}!"

    val = stack.pop()
    expected = 3

    assert val == expected, f"Got {val} but expected {expected}!"

    val = stack.max_value()
    expected = 2

    assert val == expected, f"Got max value {val} but expected {expected}!"

    val = stack.pop()
    expected = 2

    assert val == expected, f"Got {val} but expected {expected}!"

    val = stack.max_value()
    expected = 2

    assert val == expected, f"Got max value {val} but expected {expected}!"

    val = stack.pop()
    expected = 1

    assert val == expected, f"Got {val} but expected {expected}!"

    val = stack.pop()

    assert stack.is_empty(), "Stack not empty but should be!"
    assert not stack.is_full(), "Stack full but shouldn't be!"

    val = stack.pop()

    assert stack.is_empty(), "Stack not empty but should be!"
    assert not stack.is_full(), "Stack full but shouldn't be!"

    stack.push(1)
    stack.push(2)
    stack.push(2)
    stack.push(1)
    stack.push(3)
    stack.push(1)
    stack.push(4)
    stack.push(5)
    stack.push(1)
    stack.push(9)

    assert not stack.is_empty(), "Stack empty but shouldn't be!"
    assert stack.is_full(), "Stack not full but should be!"

    stack.push(9)
    stack.push(2)

    assert not stack.is_empty(), "Stack empty but shouldn't be!"
    assert stack.is_full(), "Stack not full but should be!"


if __name__ == '__main__':
    main()
