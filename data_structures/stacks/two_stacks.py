#!/usr/bin/env python3

"""
Simple implementation of two stacks using one fixed-length array (i.e. list).

Deliberately only using basic Python data structures. The two stacks are
represented by a dict, consisting of one array (i.e. Python list) and two
indices indicating the tops of the two stacks (TOSs).

Elements are not removed from the array, only the indices are updated to keep
track of the TOSs.

The first stack takes the first half of the array, the second stack takes the
second half.

The total length of the stacks is stored as `len` but could as well be derived
from the fixed-length list.
"""


def main():
    test_stacks_even()
    test_stacks_odd()


def init_stacks(length):
    return dict({
        'elements': [None] * length,
        'len': length,
        'top1': 0,
        'top2': length//2,
    })


def is_empty_stack1(stacks):
    return stacks['top1'] == 0


def is_empty_stack2(stacks):
    return stacks['top2'] == stacks['len'] // 2


def is_full_stack1(stacks):
    return stacks['top1'] == stacks['len'] // 2


def is_full_stack2(stacks):
    return stacks['top2'] == stacks['len']


def push_stack1(stacks, value):
    if is_full_stack1(stacks):
        print(f"WARNING: Cannot push value {value} to stack 1, stack is full!")
        return

    stacks['elements'][stacks['top1']] = value
    stacks['top1'] += 1


def push_stack2(stacks, value):
    if is_full_stack2(stacks):
        print(f"WARNING: Cannot push value {value} to stack 2, stack is full!")
        return

    stacks['elements'][stacks['top2']] = value
    stacks['top2'] += 1


def pop_stack1(stacks):
    if is_empty_stack1(stacks):
        print("WARNING: Cannot pop, stack 1 empty!")
        return None

    stacks['top1'] -= 1
    return stacks['elements'][stacks['top1']]


def pop_stack2(stacks):
    if is_empty_stack2(stacks):
        print("WARNING: Cannot pop, stack 2 empty!")
        return None

    stacks['top2'] -= 1
    return stacks['elements'][stacks['top2']]


def test_stacks_odd():
    stacks = init_stacks(length=5)

    print(f"stacks: {stacks}")

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    pop_stack1(stacks)
    pop_stack2(stacks)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    push_stack1(stacks, 'a')
    push_stack2(stacks, 'A')

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    push_stack1(stacks, 'b')
    push_stack2(stacks, 'B')
    push_stack2(stacks, 'C')

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert is_full_stack1(stacks), "Stack 1 is not full but should be!"
    assert is_full_stack2(stacks), "Stack 2 is not full but should be!"

    push_stack1(stacks, 'c')

    print(f"stacks: {stacks}")

    val = pop_stack1(stacks)
    expected = 'b'

    assert val == expected, f"Got {val} but expected {expected}!"

    val = pop_stack2(stacks)
    expected = 'C'

    assert val == expected, f"Got {val} but expected {expected}!"

    val = pop_stack1(stacks)
    expected = 'a'

    assert val == expected, f"Got {val} but expected {expected}!"
    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"

    val = pop_stack2(stacks)
    expected = 'B'

    assert val == expected, f"Got {val} but expected {expected}!"

    val = pop_stack2(stacks)
    expected = 'A'

    assert val == expected, f"Got {val} but expected {expected}!"
    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"


def test_stacks_even():
    stacks = init_stacks(length=12)

    print(f"stacks: {stacks}")

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    pop_stack1(stacks)
    pop_stack2(stacks)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    push_stack1(stacks, 'a')
    push_stack2(stacks, 'A')

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    push_stack1(stacks, 'b')
    push_stack2(stacks, 'B')
    push_stack1(stacks, 'c')
    push_stack2(stacks, 'C')

    print(f"stacks: {stacks}")

    push_stack1(stacks, 'd')
    push_stack2(stacks, 'D')
    push_stack1(stacks, 'e')
    push_stack2(stacks, 'E')
    push_stack1(stacks, 'f')
    push_stack2(stacks, 'F')

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert is_full_stack1(stacks), "Stack 1 is not full but should be!"
    assert is_full_stack2(stacks), "Stack 2 is not full but should be!"

    push_stack1(stacks, 'g')
    push_stack2(stacks, 'G')
    push_stack1(stacks, 'h')
    push_stack2(stacks, 'H')

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert is_full_stack1(stacks), "Stack 1 is not full but should be!"
    assert is_full_stack2(stacks), "Stack 2 is not full but should be!"

    val = pop_stack1(stacks)
    expected = 'f'

    assert val == expected, f"Got {val} but expected {expected}!"

    val = pop_stack2(stacks)
    expected = 'F'

    assert val == expected, f"Got {val} but expected {expected}!"

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"

    val = pop_stack1(stacks)
    expected = 'e'

    assert val == expected, f"Got {val} but expected {expected}!"

    val = pop_stack2(stacks)
    expected = 'E'

    assert val == expected, f"Got {val} but expected {expected}!"

    pop_stack1(stacks)
    pop_stack1(stacks)
    pop_stack1(stacks)

    val = pop_stack1(stacks)
    expected = 'a'

    assert val == expected, f"Got {val} but expected {expected}!"

    pop_stack2(stacks)
    pop_stack2(stacks)
    pop_stack2(stacks)

    val = pop_stack2(stacks)
    expected = 'A'

    assert val == expected, f"Got {val} but expected {expected}!"

    pop_stack1(stacks)
    pop_stack2(stacks)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not is_full_stack1(stacks), "Stack 1 is full but shouldn't be!"
    assert not is_full_stack2(stacks), "Stack 2 is full but shouldn't be!"


if __name__ == '__main__':
    main()
