#!/usr/bin/env python3

"""
Simple implementation of two stacks using one fixed-length array (i.e. list).

Deliberately only using basic Python data structures. The two stacks are
represented by a dict, consisting of one array (i.e. Python list) and two
indices indicating the tops of the two stacks (TOSs).

The first stack starts at the first element, the second starts at the last
element. The stacks are considered full when the two TOSs are adjacent.

The first stack is considered full when its TOS is equal to the second
stack's TOS. Conversely, the second stack is considered full when its TOS
equals the first TOS - 1. As a consequence of this, both stacks can only hold
length - 1 elements in total.

Elements are not removed from the array, only the indices are updated to keep
track of the TOSs.

The total length of the stacks is stored as `len` but could as well be derived
from the fixed-length list.
"""


def init_stacks(length):
    return dict({
        'len': length,
        'elements': [None] * length,
        'tos1': 0,
        'tos2': length - 1,
    })


def is_empty_stack1(stacks):
    return stacks['tos1'] == 0


def is_empty_stack2(stacks):
    return stacks['tos2'] == stacks['len'] - 1


def are_full(stacks):
    return stacks['tos1'] == stacks['tos2']


def push_stack1(stacks, value):
    if are_full(stacks):
        print(f"WARNING: Cannot push value {value} to stack 1, stack is full!")
        return

    stacks['elements'][stacks['tos1']] = value
    stacks['tos1'] += 1


def push_stack2(stacks, value):
    if are_full(stacks):
        print(f"WARNING: Cannot push value {value} to stack 2, stack is full!")
        return

    stacks['elements'][stacks['tos2']] = value
    stacks['tos2'] -= 1


def pop_stack1(stacks):
    if is_empty_stack1(stacks):
        print("WARNING: Cannot pop, stack 1 is empty.")
        return None

    stacks['tos1'] -= 1
    return stacks['elements'][stacks['tos1']]


def pop_stack2(stacks):
    if is_empty_stack2(stacks):
        print("WARNING: Cannot pop, stack 2 is empty.")
        return None

    stacks['tos2'] += 1
    return stacks['elements'][stacks['tos2']]


def main():
    stacks = init_stacks(length=6)

    print(f"stacks: {stacks}")

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"

    # Both stacks are empty, popping shouldn't change that.
    pop_stack1(stacks)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"

    pop_stack2(stacks)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"

    # Fill stack 1.

    push_stack1(stacks, 1)
    push_stack1(stacks, 2)
    push_stack1(stacks, 3)

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"

    push_stack1(stacks, 4)
    push_stack1(stacks, 5)

    print(f"stacks: {stacks}")

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert are_full(stacks), "Stacks are full but shouldn't be!"

    # Stacks are full, shouldn't allow to push another value.
    push_stack1(stacks, 5)

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert are_full(stacks), "Stacks are full but shouldn't be!"

    # Stack 2 is empty, popping should return None.
    val = pop_stack2(stacks)
    expected = None

    assert val == expected, f"Got {val} but expected {expected}!"

    # Stack 2 is empty but stack 1 already has 5 elements.
    push_stack2(stacks, 7)

    print(f"stacks: {stacks}")

    assert not is_empty_stack1(stacks), "Stack 1 empty but shouldn't be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert are_full(stacks), "Stacks are full but shouldn't be!"

    # Popping all values in stack 1.
    pop_stack1(stacks)

    assert not are_full(stacks), "Stack are full but shouldn't be!"

    pop_stack1(stacks)
    pop_stack1(stacks)
    pop_stack1(stacks)

    val = pop_stack1(stacks)
    expected = 1

    assert val == expected, f"Got {val} but expected {expected}!"

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"

    # Stack 1 is empty now, popping should return None.
    val = pop_stack1(stacks)
    expected = None

    assert val == expected, f"Got {val} but expected {expected}!"

    push_stack2(stacks, 1)
    push_stack2(stacks, 2)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert not is_empty_stack2(stacks), "Stack 2  empty but shouldn't be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"

    push_stack2(stacks, 3)
    push_stack2(stacks, 4)
    push_stack2(stacks, 5)

    print(f"stacks: {stacks}")

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert are_full(stacks), "Stacks are full but shouldn't be!"

    # Stacks are full, shouldn't allow to push another value.
    push_stack2(stacks, 5)

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert not is_empty_stack2(stacks), "Stack 2 empty but shouldn't be!"
    assert are_full(stacks), "Stacks are full but shouldn't be!"

    # Popping all values in stack 2.
    pop_stack2(stacks)

    assert not are_full(stacks), "Stack are full but shouldn't be!"

    pop_stack2(stacks)
    pop_stack2(stacks)
    pop_stack2(stacks)

    val = pop_stack2(stacks)
    expected = 1

    assert val == expected, f"Got {val} but expected {expected}!"

    assert is_empty_stack1(stacks), "Stack 1 not empty but should be!"
    assert is_empty_stack2(stacks), "Stack 2 not empty but should be!"
    assert not are_full(stacks), "Stack are full but shouldn't be!"


if __name__ == '__main__':
    main()
